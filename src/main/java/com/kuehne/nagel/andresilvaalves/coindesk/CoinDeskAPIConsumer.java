package com.kuehne.nagel.andresilvaalves.coindesk;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.kuehne.nagel.andresilvaalves.coindesk.model.CurrentPrice;
import com.kuehne.nagel.andresilvaalves.coindesk.model.HistoricalBPI;

@Component
public class CoinDeskAPIConsumer {

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${COINDESK_URL}")
	private String COINDESK_URL;
	
	@Value("${HISTORICAL_URL}")
	private String HISTORICAL_URL;
	
	@Value("${CURRENTPRICE_URL}")
	private String CURRENTPRICE_URL;

	@Value("${CURRENTPRICEBYCODE_URL}")
	private String CURRENTPRICEBYCODE_URL;
	
	
	
	public CurrentPrice currentPrice() {

		StringBuffer url = new StringBuffer(COINDESK_URL);
		url.append(CURRENTPRICE_URL);

		CurrentPrice currentPrice = restTemplate.getForObject(url.toString(), CurrentPrice.class);
		
		return currentPrice;
	}
	
	
	public CurrentPrice currentPriceByCode(final String currencyCode) {

		StringBuffer sb = new StringBuffer(COINDESK_URL);
		sb.append(CURRENTPRICEBYCODE_URL);
		String url = StringUtils.replace(sb.toString(), "##", currencyCode);

		CurrentPrice currentPrice = restTemplate.getForObject(url, CurrentPrice.class);
		
		return currentPrice;
	}
	
	public HistoricalBPI historicalPrice(final String currencyCode) {
		
		StringBuffer url = new StringBuffer(COINDESK_URL);
		url.append(HISTORICAL_URL);
		url.append(StringUtils.isNotBlank(currencyCode) ? "?currency="+currencyCode : StringUtils.EMPTY);
		
		HistoricalBPI historicalPrice = restTemplate.getForObject(url.toString(), HistoricalBPI.class);
		
		return historicalPrice;
	}
	

}
