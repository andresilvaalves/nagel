package com.kuehne.nagel.andresilvaalves.coindesk.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Currency {

	private String code;
	private String symbol;
	private String rate;
	private String description;
	private Float rate_float;

}
