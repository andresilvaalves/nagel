package com.kuehne.nagel.andresilvaalves.coindesk.model;

import java.util.Map;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class HistoricalBPI {

	private Time time;
	private String disclaimer;
	private Map<String, Float> bpi;
}
