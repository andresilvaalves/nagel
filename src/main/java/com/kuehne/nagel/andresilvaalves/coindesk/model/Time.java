package com.kuehne.nagel.andresilvaalves.coindesk.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Time {

	private String updated;
	private String updatedISO;
	private String updateduk;

}
