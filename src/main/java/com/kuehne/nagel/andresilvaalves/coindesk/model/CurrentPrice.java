package com.kuehne.nagel.andresilvaalves.coindesk.model;

import java.util.Map;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CurrentPrice {

	private Time time;
	private String disclaimer;
	private String chartName;
	private Map<String, Currency> bpi;
}
