package com.kuehne.nagel.andresilvaalves.service;

import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.kuehne.nagel.andresilvaalves.coindesk.CoinDeskAPIConsumer;
import com.kuehne.nagel.andresilvaalves.coindesk.model.Currency;
import com.kuehne.nagel.andresilvaalves.coindesk.model.CurrentPrice;
import com.kuehne.nagel.andresilvaalves.coindesk.model.HistoricalBPI;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class NagelService {
	
	@Autowired
	private CoinDeskAPIConsumer coinDeskConsumer;

	public void searchCurrencyInformation(){
		
		try {
			Scanner in = new Scanner(System.in);
			System.out.println("Enter the currency code: ");

			String currencyCode = StringUtils.upperCase(in.nextLine());
			in.close();

			
			CurrentPrice currentPrice = coinDeskConsumer.currentPriceByCode(currencyCode);
			
			Currency currency = currentPrice.getBpi().get(currencyCode);
			System.out.println("Requested Currency: " + currency.getCode() +"- "+currency.getDescription());
			System.out.println("Current Bitcoin rate: "+currency.getRate());
			
			HistoricalBPI historicalPrice = coinDeskConsumer.historicalPrice(currencyCode);
			
			Float lowest = historicalPrice.getBpi().values().stream().min(Float::compare).get();
			Float highest = historicalPrice.getBpi().values().stream().max(Float::compare).get();
			
			System.out.println("Lowest Bitcoin rate in the last 30 days: "+String.format("%.04f", lowest));
			System.out.println("Highest Bitcoin rate in the last 30 days: "+String.format("%.04f", highest));
			
		} catch (HttpClientErrorException e) {
			log.error(e.getResponseBodyAsString());
		} catch (Exception e) {
			log.error(e.getLocalizedMessage());
		}
	}
	
}
