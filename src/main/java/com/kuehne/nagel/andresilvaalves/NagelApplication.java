package com.kuehne.nagel.andresilvaalves;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

import com.kuehne.nagel.andresilvaalves.coindesk.JavaScriptMessageConverter;
import com.kuehne.nagel.andresilvaalves.service.NagelService;

@SpringBootApplication
public class NagelApplication {

	@Autowired
	private NagelService nagelService;

	public static void main(String[] args) {
		SpringApplication.run(NagelApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		RestTemplate restTemplate = builder.build();
		restTemplate.getMessageConverters().add(new JavaScriptMessageConverter());
		return restTemplate;
	}

	@Bean
	@Profile("!test")
	public CommandLineRunner run() throws Exception {
		return args -> {

			nagelService.searchCurrencyInformation();

		};
	}

}
