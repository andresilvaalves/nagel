package com.kuehne.nagel.andresilvaalves;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class NagelApplicationTests {

	@Test
	public void contextLoads() {
	}
}
