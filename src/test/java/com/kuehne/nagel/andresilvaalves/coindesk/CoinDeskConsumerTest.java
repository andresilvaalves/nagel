package com.kuehne.nagel.andresilvaalves.coindesk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import com.kuehne.nagel.andresilvaalves.coindesk.CoinDeskAPIConsumer;
import com.kuehne.nagel.andresilvaalves.coindesk.model.CurrentPrice;
import com.kuehne.nagel.andresilvaalves.coindesk.model.HistoricalBPI;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class CoinDeskConsumerTest {

	private List<String> currencyCodes;

	@Autowired(required = true)
	private CoinDeskAPIConsumer coinDeskConsumer;

	@Before
	public void setUp() {
		currencyCodes = Stream.of("USD", "EUR", "GBP", "BRL", "EEK").collect(Collectors.toList());
	}

	
	@Test
	public void currentPrice() {
		CurrentPrice currentPrice = coinDeskConsumer.currentPrice();
		assertNotNull(currentPrice.getBpi());
	}
	
	@Test
	public void currentPriceByCode() {
		currencyCodes.stream().forEach(currencyCode -> {

			CurrentPrice currentPrice = coinDeskConsumer.currentPriceByCode(currencyCode);

			assertEquals(currentPrice.getBpi().get(currencyCode).getCode(), currencyCode);

		});
	}

	@Test
	public void historicalPrice() {
		currencyCodes.stream().forEach(currencyCode -> {

			HistoricalBPI historicalPrice = coinDeskConsumer.historicalPrice(currencyCode);
			assertNotNull(historicalPrice);
			assertNotNull(historicalPrice.getBpi());
			assertTrue(historicalPrice.getBpi().size() > 1);

		});
	}
	
	@Test(expected = HttpClientErrorException.class)
	public void currentPriceWrongCode() {
		coinDeskConsumer.currentPriceByCode("NOT");
	}

}
